from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_valid_id():
    response = client.get("/items/1")
    assert response.status_code == 200
    assert response.json() == {"shovel": "a tool to dig"}

def test_invalid_id():
    response = client.get("/items/4")
    assert response.status_code == 404
    assert response.json() == {"detail": "Item not found"}

def test_valid_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "api root"}

def test_valid_create():
    response = client.post("/items/4", json={"item name": "saw", "item_description": "a tool to cut"})
    assert response.status_code == 201
    assert response.json() == {"item name": "saw", "item_description": "a tool to cut"}

def test_invalid_create():
    response = client.post("/items/1", json={"item name": "saw", "item_description": "a tool to cut"})
    assert response.status_code == 400
    assert response.json() == {"detail": "Item already exists"}

def test_valid_delete():
    response = client.delete("/items/1")
    assert response.status_code == 200
    assert response.json() == {"detail": "Item deleted"}

def test_invalid_delete():
    response = client.get("/items/5")
    assert response.status_code == 404
    assert response.json() == {"detail": "Item not found"}

def test_valid_update():
    response = client.put("/items/4", json={"item name": "dildo", "item_description": "a tool to beat somebody"})
    assert response.status_code == 200
    assert response.json() == {"item name": "dildo", "item_description": "a tool to beat somebody"}

def test_invalid_update():
    response = client.put("/items/6", json={"item name": "saw", "item_description": "a tool to cut"})
    assert response.status_code == 404
    assert response.json() == {"detail": "Item not found"}