from fastapi import FastAPI
from fastapi import HTTPException
from fastapi import status, Response
import uvicorn

app = FastAPI()

inventory_items = {
    1: {'shovel': 'a tool to dig'},
    2: {'hammer': 'a tool to hit'},
    3: {'screwdriver': 'a tool to screw'},
}


@app.get("/")
async def root():
    return {"message": "api root"}


@app.get("/items/{slot_id}")
async def read_item(slot_id: int):
    if slot_id not in inventory_items:
        raise HTTPException(status_code=404, detail="Item not found")
    return inventory_items[slot_id]


@app.post("/items/{slot_id}")
async def create_item(slot_id: int, item: dict, response: Response):
    if slot_id in inventory_items:
        raise HTTPException(status_code=400, detail="Item already exists")
    inventory_items[slot_id] = item
    response.status_code = status.HTTP_201_CREATED
    return inventory_items[slot_id]


@app.delete("/items/{slot_id}")
async def delete_item(slot_id: int):
    if slot_id not in inventory_items:
        return {"detail": "Item not found"}
    del inventory_items[slot_id]
    return {"detail": "Item deleted"}


@app.put("/items/{slot_id}")
async def update_item(slot_id: int, item: dict):
    if slot_id not in inventory_items:
        raise HTTPException(status_code=404, detail="Item not found")
    inventory_items[slot_id] = item
    return inventory_items[slot_id]


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)